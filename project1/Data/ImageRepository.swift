//
//  FileRepository.swift
//  project1
//
//  Created by Iqbal Rahman on 07/03/24.
//

import Foundation
import UIKit

class ImageManager {
//    let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    let fileURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("savedImage.jpg")
    static let shared = ImageManager()
    
    private init() {
        
    }
    
    func saveImage(_ imageToSave: UIImage) {
        fileURL.saveImage(imageToSave)
        print(fileURL.path())
    }
    
    func loadImage() -> UIImage? {
        var loadedImage: UIImage? = nil
        fileURL.loadImage(&loadedImage)
        return loadedImage
    }
}

