import Foundation
import Alamofire

//datasource
struct GetAllData {
    func getRestaurant(URL: String, completion: @escaping ([RestaurantElement]?) -> Void) {
        AF.request(URL).responseData { response in
            guard let data = response.data, response.error == nil else { return }
            
            let decoder = JSONDecoder()
            
            do {
                let restaurant = try decoder.decode(Restaurant.self, from: data)
                completion(restaurant.restaurants)
            } catch {
                print("Decoding error: \(error)")
                completion([])
            }
        }
    }
}
