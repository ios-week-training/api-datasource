import Foundation
import UIKit

// MARK: - Restaurant
struct Restaurant: Codable {
    let error: Bool
    let message: String
    let count: Int
    let restaurants: [RestaurantElement]
}

// MARK: - RestaurantElement
struct RestaurantElement: Codable, Identifiable {
    let id, name, description, pictureID: String
    let city: String
    let rating: Double

    enum CodingKeys: String, CodingKey {
        case id, name, description
        case pictureID = "pictureId"
        case city, rating
    }
}

struct RestaurantViewModel: Identifiable {
    let id, name, description, pictureID: String    
    let city: String
    let rating: Double
    let image: UIImage?
    
    init(from restaurantElement: RestaurantElement, image: UIImage? = nil) {
        self.id = restaurantElement.id
        self.name = restaurantElement.name
        self.description = restaurantElement.description
        self.pictureID = restaurantElement.pictureID
        self.city = restaurantElement.city
        self.rating = restaurantElement.rating
        self.image = image 
    }
}
