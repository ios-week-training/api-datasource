import Foundation
import Combine

class TabSelectionViewModel: ObservableObject {
    @Published var selectedTab: TabsNameEnum = .Home
}
