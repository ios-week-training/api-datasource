//
//  CameraScreen.swift
//  project1
//
//  Created by Iqbal Rahman on 07/03/24.
//

import SwiftUI

struct CameraScreen: View {
    @State var takePicture: Bool = false
    @State var imageShown: UIImage?
    @StateObject var imagePresenter: ImagePresenter = ImagePresenter()
    
    var body: some View {
        VStack {
            if let selectedImg = imagePresenter.imageloaded {
                Image(uiImage: selectedImg)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 390, height: 320)
                    .clipped()
            }
            Text("take a picture")
                .onTapGesture {
                    self.takePicture.toggle()
                }
                .fullScreenCover(isPresented: self.$takePicture) {
                    CameraPresenter(selectedImage: self.$imageShown) {image in 
                        imagePresenter.setImage(image)
                    }
                }
        }
    }
}

#Preview {
    CameraScreen()
}
