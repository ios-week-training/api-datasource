//
//  ContentView.swift
//  project1
//
//  Created by Iqbal Rahman on 06/03/24.
//

import SwiftUI

enum TabsNameEnum: String {
    case Home, Maps, Picture
}

struct ContentView: View {
    @StateObject private var viewModel = TabSelectionViewModel()
    
    var body: some View {
        NavigationView {
            TabView(selection: $viewModel.selectedTab) {
                Group {
                    HomeScreen()
                    .tabItem {
                        Image(systemName: "house.fill")
                        Text("Home")
                    }
                    .tag(TabsNameEnum.Home)
                    
                    MapScreen(viewModel: viewModel)
                    .tabItem {
                        Image(systemName: "map.fill")
                        Text("Map")
                    }
                    .tag(TabsNameEnum.Maps)
                    
                    CameraScreen()
                    .tabItem {
                        Image(systemName: "camera.fill")
                        Text("Picture")
                    }
                    .tag(TabsNameEnum.Picture)
                }
            }
            .navigationTitle(viewModel.selectedTab.rawValue)
        }
    }
}

#Preview {
    ContentView()
}
 
