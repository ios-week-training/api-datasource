//
//  HomeScreen.swift
//  project1
//
//  Created by Iqbal Rahman on 06/03/24.
//

import SwiftUI

struct HomeScreen: View {
    @StateObject var homePresenter: HomePresenter = HomePresenter()
    
    var body: some View {
        VStack {
            ScrollView {
                LazyVStack {
                    if !homePresenter.restaurantList.isEmpty {
                        ForEach(homePresenter.restaurantList) { item in
                            ListAdaptor(person: item)
                        }
                    } else {
                        HStack {
                            ProgressView()
                                .progressViewStyle(.circular)
                            Text("Loading ...")
                        }
                    }
                }
            }
            .padding()
        }
        .environmentObject(homePresenter)
    }
}

#Preview {
    HomeScreen()
}
