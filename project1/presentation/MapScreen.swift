//
//  MapScreen.swift
//  project1
//
//  Created by Iqbal Rahman on 07/03/24.
//

import SwiftUI
import MapKit

struct MapScreen: View {
    @StateObject var locationManager: MapPresenter = .init()
    @ObservedObject var viewModel: TabSelectionViewModel
    @State var showMap: Bool = false
    
    var body: some View {
        VStack {
            if !locationManager.isLocationDetermined && !locationManager.isLocationUnAuthorized{
                LoadingMap(viewModel: viewModel, statusLoading: 0)
            } else if locationManager.isLocationUnAuthorized && viewModel.selectedTab == .Maps{
                LoadingMap(viewModel: viewModel, statusLoading: 1)
            } else {
                Map(coordinateRegion: locationManager.binding,showsUserLocation: true)
                    .edgesIgnoringSafeArea(.all)
                    .clipped()
            
            }
        }
    }
}

#Preview {
    MapScreen(viewModel: TabSelectionViewModel())
}
