//
//  DetailAdaptor.swift
//  project1
//
//  Created by Iqbal Rahman on 07/03/24.
//

import SwiftUI

struct DetailAdaptor: View {
    private let person: RestaurantViewModel
    @State private var showCamera = false
    @State private var selectedImage: UIImage?
    
    init(person: RestaurantViewModel) {
        self.person = person
    }
    
    var body: some View {
        ScrollView {
            VStack {
                ZStack(alignment: .top) {
                    if let selectedImage = selectedImage {
                        Image(uiImage: selectedImage)
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                            .frame(width: 390, height: 320)
                            .clipped()
                    } else {
                        ImageBig(person.pictureID)
                    }
                }
                .frame(width: 390, height: 320)
                .clipped()
                .overlay(alignment: .bottomTrailing) {
                    Image(systemName: "pencil.circle.fill")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 40)
                        .padding()
                        .onTapGesture {
                            self.showCamera.toggle()
                        }
                }
                .fullScreenCover(isPresented: self.$showCamera) {
                    CameraPresenter(selectedImage: self.$selectedImage) {image in 
                        
                    }
                }
                VStack(alignment: .leading, spacing: 4) {
                    HStack(alignment: .firstTextBaseline) {
                        Text(person.name)
                            .font(.system(size: 29, weight: .semibold, design: .default))
                        Spacer()
                        HStack(alignment: .firstTextBaseline, spacing: 3) {
                            Image(systemName: "star.fill")
                                .symbolRenderingMode(.multicolor)
                            Text("\(String(format: "%.1f", person.rating))")
                                .foregroundColor(.secondary)
                        }
                        .font(.system(.subheadline, weight: .medium))
                    }
                    Text(person.city)
                        .font(.system(.callout, weight: .medium))
                    Text(person.description)
                        .font(.system(.callout).width(.condensed))
                        .padding(.vertical)
                }
                .padding(.horizontal, 24)
                .padding(.top, 12)
            }
        }
    }
}

#Preview {
    DetailAdaptor(
        person: RestaurantViewModel(from: RestaurantElement(id: "id", name: "Name", description: "Lorem", pictureID: "14", city: "city", rating: 14.4))
    )
}
