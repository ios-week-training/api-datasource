//
//  ImageSmall.swift
//  project1
//
//  Created by Iqbal Rahman on 07/03/24.
//

import SwiftUI

struct ImageBig: View {
    private let imageUrl: String =  "https://restaurant-api.dicoding.dev/images/large/"
    private let imageID : String
    
    init(_ imageID: String) {
        self.imageID = imageID
    }
    
    var body: some View {
        AsyncImage(url: URL(string: "\(imageUrl)\(imageID)")) { phase in
            switch phase {
                case .empty:
                    ProgressView()
                case .success(let image):
                    image
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 390, height: 320)
                    .clipped()
                case .failure:
                    Image(systemName: "photo")
                         .foregroundColor(.gray)
                @unknown default:
                    EmptyView()
            }
        }

    }
}

#Preview {
    ImageBig("14")
}
