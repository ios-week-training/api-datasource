//
//  ImageSmall.swift
//  project1
//
//  Created by Iqbal Rahman on 07/03/24.
//

import SwiftUI

struct ImageSmall: View {
    private let imageUrl: String =  "https://restaurant-api.dicoding.dev/images/small/"
    private let imageID : String
    
    init(_ imageID: String) {
        self.imageID = imageID
    }
    
    var body: some View {
        AsyncImage(url: URL(string: "\(imageUrl)\(imageID)")) { phase in
            switch phase {
                case .empty:
                    ProgressView()
                case .success(let image):
                    image
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 70, height: 70)
                    .clipped()
                    .mask { RoundedRectangle(cornerRadius: 8, style: .continuous) }
                case .failure:
                    Image(systemName: "photo")
                         .foregroundColor(.gray)
                @unknown default:
                    EmptyView()
            }
        }

    }
}

#Preview {
    ImageSmall("14")
}
