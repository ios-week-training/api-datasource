//
//  listAdaptor.swift
//  app1
//
//  Created by Iqbal Rahman on 05/03/24.
//

import SwiftUI

struct ListAdaptor: View {
    private let person: RestaurantViewModel
    
    init(person: RestaurantViewModel) {
        self.person = person
    }
    
    var body: some View {
        HStack(spacing: 10) {
            ImageSmall(person.pictureID)
            VStack(alignment: .leading) {
                Text(person.name)
                    .font(.system(size: 16, weight: .medium, design: .default))
                Text(person.city)
                    .font(.footnote)
                    .foregroundColor(.secondary)
                HStack {
                    Image(systemName: "star.fill")
                        .foregroundColor(.yellow)
                    Text("\(String(format: "%.1f", person.rating))")
                        .font(.footnote)
                        .foregroundColor(.secondary)
                }
            }
            .font(.subheadline)
            Spacer()
            NavigationLink {
                DetailAdaptor(person: person)
            } label: {
                Image(systemName: "ellipsis")
                    .foregroundColor(Color(.displayP3, red: 234/255, green: 76/255, blue: 97/255))
                    .font(.title3)
            }
        }
        Divider()
    }
}

#Preview {
    ListAdaptor(
        person: RestaurantViewModel(from: RestaurantElement(id: "id", name: "Name", description: "Lorem", pictureID: "14", city: "city", rating: 14.4))
    )
}
