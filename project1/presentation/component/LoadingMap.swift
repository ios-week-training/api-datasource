//
//  LoadingMap.swift
//  project1
//
//  Created by Iqbal Rahman on 07/03/24.
//

import SwiftUI

struct LoadingMap: View {
    @ObservedObject var viewModel: TabSelectionViewModel
    private var statusLoading: Int
    
    init(viewModel: TabSelectionViewModel, statusLoading: Int) {
        self.viewModel = viewModel
        self.statusLoading = statusLoading
    }
    
    var body: some View {
        switch statusLoading {
        case 0:
            ProgressView()
                .progressViewStyle(.circular)
            Text("Loading ...")
        case 1:
            ProgressView()
                .progressViewStyle(.circular)
            Text("Loading ...")
            .alert("This App Need Location Permissions", isPresented: .constant(true)) {
                Button("Grant Permissions", role: .none) {
                    guard let url = URL(string: UIApplication.openSettingsURLString) else { return }
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
                Button("Cancel", role: .cancel) {
                    viewModel.selectedTab = .Home
                }
            }
        default:
            ProgressView()
                .progressViewStyle(.circular)
            Text("Loading ...")
        }
    }
}

#Preview {
    LoadingMap(viewModel: TabSelectionViewModel(), statusLoading: 0)
}
