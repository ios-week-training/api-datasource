import Foundation
import SwiftUI

class HomePresenter: ObservableObject {
    private let allData = GetAllData()
    private let url = "https://restaurant-api.dicoding.dev/list"
    @Published var restaurantList: [RestaurantViewModel] = []
    
    init() {
        fetchRestaurantData()
    }
    
    private func fetchRestaurantData() {
        allData.getRestaurant(URL: url) { restaurantResult in
            guard let restaurants = restaurantResult else { return }
            DispatchQueue.main.async {
                self.restaurantList = restaurants.map { RestaurantViewModel(from: $0) }
            }
        }
    }
}
