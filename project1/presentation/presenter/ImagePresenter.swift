//
//  ImagePresenter.swift
//  project1
//
//  Created by Iqbal Rahman on 07/03/24.
//

import Foundation
import UIKit

class ImagePresenter: ObservableObject {
    @Published var imageloaded: UIImage?
    private let imageManager = ImageManager.shared
    
    init() {
        getImage()
    }
    
    func getImage() {
        imageloaded = imageManager.loadImage()
    }
    
    func setImage(_ imageSaved: UIImage) {
        imageManager.saveImage(imageSaved)
        getImage()
    }
    
}
