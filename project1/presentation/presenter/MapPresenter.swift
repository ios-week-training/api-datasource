import CoreLocation
import SwiftUI
import MapKit

final class MapPresenter: NSObject, CLLocationManagerDelegate, ObservableObject, MKMapViewDelegate {
    @Published var manager: CLLocationManager = .init()
    @Published var isLocationUnAuthorized: Bool = false
    @Published var isLocationDetermined: Bool = false
    @Published var mapRegion = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 43.457105, longitude: -80.508361), span: MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2))
    
    var binding: Binding<MKCoordinateRegion> {
        Binding {
            self.mapRegion
        } set: { newRegion in
            DispatchQueue.main.async {
                self.mapRegion = newRegion
            }
        }
    }
    
    override init() {
        super.init()
        
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        manager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        checkLocationAuthorization()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = manager.location {
            DispatchQueue.main.async {
                self.mapRegion = MKCoordinateRegion(center: location.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2))
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    }
    
    func checkLocationAuthorization() {
        switch manager.authorizationStatus {
        case .notDetermined:
            print("not determined")
            DispatchQueue.main.async {
                self.manager.requestWhenInUseAuthorization()
            }
            isLocationDetermined = true
        case .restricted, .denied:
            isLocationUnAuthorized = true
            print("Location Authorization Denied or Restricted")
            DispatchQueue.main.async {
                self.manager.requestWhenInUseAuthorization()
            }
        case .authorizedWhenInUse, .authorizedAlways:
            print("auth always")
            isLocationDetermined = true
            isLocationUnAuthorized = false
            DispatchQueue.main.async {
                self.manager.startUpdatingLocation()
            }
        default:
            print("default")
            break
        }
    }
}
