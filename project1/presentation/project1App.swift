//
//  project1App.swift
//  project1
//
//  Created by Iqbal Rahman on 06/03/24.
//

import SwiftUI

@main
struct project1App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
